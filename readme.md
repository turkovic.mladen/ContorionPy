
# ContorionPy Project 

## Goal
Attempt to analyse and predict the sales i.e. 'net_revenue' value from the Contorion dataset

## Code

Python code available from: git@gitlab.com:turkovic.mladen/ContorionPy.git

## Run

By running main.py the following process takes place:

### 1) Loading data train and test dataset (loader.py)
The train_data.csv dataset was preprocessed by removing all space characters with Regex (to simplify loading into Python)

### 2) Basic description
Running a small method printing out a basic description of the data loaded, separately for training and test datasets (explorer.py)

### 3)transforming the data (explorer.py)
* replacing the low-frequency (defined as not in top-n classes) nominal values from variables cat1, cat2 and cat3 with a default low-frequency label, here I use "val_LowFq"
	* the exact number of the top-categories (by frequency) can be specified at function call
* binning these nominal values to separate value columns
	
* transforming the "day_id" variable into day-of-the-week
	
* construct the feature-vector array for training and testing data and return labels separately

### 4) Training models, reporting on accuracy and saving models (trainer.py)
*Classification: Based on presumption that it is important to predict if a sale took place or not (using 'net_revenue' > 0 as boolean)
 * training classifiers sklearn.naive_bayes "MultinomialNB", "GaussianNB" and "BernoulliNB"
  * reporting on Accuracy, confusion matrix report; plus precision, recall, f1-score and support for each of them (see printout below)
		
*Regression: Attempting to predict the value of the sale. A better approach would be to first build a more accurate classifier predicting if a sale took place or not and then on the selected entries attempt the regression
 * training sklearn.linear_model "LogisticRegression" model
  * reporting "Score", "Mean Squared Error", "Mean Absolute Error", "Explained Variance Score" and "R^2 score"

### 5) Predictions are made from the Test data

* All models are used to generate distinct predictions
	* Classifiers made output 0 (no sale) or 1 (sale took place i.e. 'net_revenue' > 0)
	* Regression unfortunately predicted all 0.0 :o(

## Discussion
After performing a basic investigation in R and WEKA (Java) environment I decided to attempt developing the code in Python.

### Nominal variables 
Variables cat1, 2, 3 contain very large number of very rare categories.
Therefore the simplest approach in order to avoid losing all this data is to binarise into separate variable the most frequent values, while others are aggregated into newly created "Low Frequency" value

### Day ID
Since we have only two months worth of data it made no sense to attempt building monthly-cicle detection features therefore I built only the basic week-day (0-6) encoder
 
### net_revenue
Due to the sparseness of this value (0.8%) I also attempted to use a classification approach in order to try to detect a successful sale.
Once the sale takes place the value is reasonably high therefore this might be a good thing to try to predict.
Logistic regression approach to this problem simply collapsed to predicting zeroes only. Presumably by balancing the data (e.g. 1:5) of sale to non-sale a better result could be achieved
 
### Time to develop
Since this is the first time I developed anything in Python it took me significantly more than 2-3 hours 
 

## Screenshoot

		C:\Users\Mladen\MyPrograms\Python-2-7-12\python.exe C:/Users/Mladen/PycharmProjects/Contorion/src/main.py
		Loading:  data/train_data2.csv.npy
		Loading:  data/test_data.csv.npy

		Train data description:
		Type: <type 'numpy.ndarray'>
		Shape: (169181L,)
		Column names: ('net_price', 'day_id', 'num1', 'num2', 'num3', 'num4', 'num5', 'num6', 'cat1', 'cat2', 'cat3', 'net_revenue')

		 Statistical overview:
		num1 DescribeResult(nobs=169181L, minmax=(0, 1), mean=0.20547224570134945, variance=0.16325436691647766, skewness=1.4578911568142217, kurtosis=0.12544662511710936)
		num2 DescribeResult(nobs=169181L, minmax=(0.52000000000000002, 11.68), mean=1.0866969695178534, variance=0.010794712930229764, skewness=8.686741115418654, kurtosis=657.832937112699)
		num3 DescribeResult(nobs=169181L, minmax=(0.28999999999999998, 2.4100000000000001), mean=0.98103528173967525, variance=0.0065045677408871972, skewness=1.4855496802086285, kurtosis=7.939151804578238)
		num4 DescribeResult(nobs=169181L, minmax=(0.0, 402528.0), mean=159.72616310342178, variance=10630092.008899892, skewness=121.9579676138227, kurtosis=15033.30238775747)
		num5 DescribeResult(nobs=169181L, minmax=(2.9488558588300001e-05, 1148.6800000000001), mean=2.4208185312789965, variance=48.546290120577261, skewness=86.24949319524512, kurtosis=12540.543345837725)
		num6 DescribeResult(nobs=169181L, minmax=(1, 35), mean=8.8296144366093117, variance=13.752479405069749, skewness=0.29891616425849227, kurtosis=0.664419944825613)
		net_revenue > 0: 1499

		Test data description:
		Type: <type 'numpy.ndarray'>
		Shape: (97486L,)
		Column names: ('net_price', 'day_id', 'num1', 'num2', 'num3', 'num4', 'num5', 'num6', 'cat1', 'cat2', 'cat3')

		 Statistical overview:
		num1 DescribeResult(nobs=97486L, minmax=(0, 1), mean=0.21322035984654206, variance=0.16775915884730752, skewness=1.4003526535053301, kurtosis=-0.039012445820580055)
		num2 DescribeResult(nobs=97486L, minmax=(0.65000000000000002, 8.2599999999999998), mean=1.0775229263689146, variance=0.013768770952039445, skewness=13.993576489981054, kurtosis=736.2503488042934)
		num3 DescribeResult(nobs=97486L, minmax=(0.64000000000000001, 8.1199999999999992), mean=0.97392661510370726, variance=0.0091381159255726728, skewness=23.134903854719116, kurtosis=1617.984899984742)
		num4 DescribeResult(nobs=97486L, minmax=(24.0, 403512.0), mean=168.78903637445376, variance=5229722.0665673884, skewness=168.99877458033296, kurtosis=29777.915534343883)
		num5 DescribeResult(nobs=97486L, minmax=(4.3765628368900002e-05, 68.319999999999993), mean=1.8095892420993593, variance=10.493638966756716, skewness=5.663561803000695, kurtosis=55.85299047219739)
		num6 DescribeResult(nobs=97486L, minmax=(1, 34), mean=7.9513571179451406, variance=12.30871760222902, skewness=0.27102636685623166, kurtosis=0.08116558481951186)

		Binning cat1...

		Binning cat2...

		Binning cat3...

		Training MultinomialNB classifier. Please wait...
		data_train_X (152263L, 41L)
		data_train_y (152263L,)
		Saving classification model as model/MultinomialNB.pkl  ...
		 Accuracy score:  0.346672183473

		Confusion matrix
		   True  False
		[[  119    68]
		 [10985  5746]]
		Classification report:
					 precision    recall  f1-score   support

			   True       0.99      0.34      0.51     16731
			  False       0.01      0.64      0.02       187

		avg / total       0.98      0.35      0.50     16918


		Training GaussianNB classifier. Please wait...
		data_train_X (152263L, 41L)
		data_train_y (152263L,)
		Saving classification model as model/GaussianNB.pkl  ...
		 Accuracy score:  0.0761319304882

		Confusion matrix
		   True  False
		[[  183     4]
		 [15626  1105]]
		Classification report:
					 precision    recall  f1-score   support

			   True       1.00      0.07      0.12     16731
			  False       0.01      0.98      0.02       187

		avg / total       0.99      0.08      0.12     16918


		Training BernoulliNB classifier. Please wait...
		data_train_X (152263L, 41L)
		data_train_y (152263L,)
		Saving classification model as model/BernoulliNB.pkl  ...
		 Accuracy score:  0.798084880009

		Confusion matrix
		   True  False
		[[   26   161]
		 [ 3255 13476]]
		Classification report:
					 precision    recall  f1-score   support

			   True       0.99      0.81      0.89     16731
			  False       0.01      0.14      0.01       187

		avg / total       0.98      0.80      0.88     16918


		Training LogisticRegression. Please wait...
		Saving regression model as model/LogisticRegression.pkl  ...
		 Score:  0.988946684005
		 mean_squared_error:  0.0110533159948
		 mean_absolute_error:  0.0110533159948
		 explained_variance_score:  0.0
		 R^2 score:  -0.0111768573307


		Making predictions for LogisticRegression model. Please wait...
		0

		Making predictions for BernoulliNB model. Please wait...
		1338

		Making predictions for GaussianNB model. Please wait...
		63569

		Making predictions for MultinomialNB model. Please wait...
		43590

		Process finished with exit code 0
