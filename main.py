# main code

# Presumes the folder data on project level contains files
# - train_data.csv and
# - test_data.csv

# 1 load data,
# 2 explore,
# 3 transform to feature-vectors,
# 4 run machine learning, save model
# 5 make predictions for test file

from sklearn.naive_bayes import MultinomialNB, GaussianNB, BernoulliNB
from sklearn.linear_model import LogisticRegression
import numpy as np

import loader as ld
import explorer as ex
import trainer as train


# ######################################## #


def split_train_test(features, labels, training_share):

    train_f = features[:int(round(labels.size * training_share)), ]
    train_l = labels[:int(round(labels.size * training_share))]
    test_f = features[int(round(labels.size * training_share)):, ]
    test_l = labels[int(round(labels.size * training_share)):]

    return train_f, train_l, test_f, test_l

# ######################################## #

train_data = ld.get_file("data/train_data2.csv", False)
test_data = ld.get_predict_file("data/test_data.csv", False)

print "\nTrain data description: "
ex.describe(train_data)

print "\nTest data description: "
ex.describe(test_data)

train_data_features, train_data_labels, test_data_features =\
    ex.transform(train_data, test_data, 10)

# Predict if sale was made or not

train_sales_label = train_data_labels > 0

# train/test 90/10% split
train_X, train_y, test_X, test_y = \
    split_train_test(train_data_features, train_sales_label, 0.9)

# training models, reporting on accuracy and saving models
print
print "Training MultinomialNB classifier. Please wait..."
clf = MultinomialNB()
train.clasiffier(clf, train_X, train_y, test_X, test_y, "MultinomialNB")

print
print "Training GaussianNB classifier. Please wait..."
clf = GaussianNB()
train.clasiffier(clf, train_X, train_y, test_X, test_y, "GaussianNB")

print
print "Training BernoulliNB classifier. Please wait..."
clf = BernoulliNB()
train.clasiffier(clf, train_X, train_y, test_X, test_y, "BernoulliNB")


# Attempt regression

print
print "Training LogisticRegression. Please wait..."
reg = LogisticRegression()
train.regressor(reg, train_X, train_y.astype(long), test_X, test_y.astype(long),
                "LogisticRegression")

# ###################################################### #

# Make predictions by using available models

model_name = "LogisticRegression"
predictions = train.predict(model_name, test_data_features)
print sum(predictions)
np.savetxt("data/test_data_" + model_name + "_prediction.csv", predictions,
           delimiter=",")

# --------------------------------------- #
model_name = "BernoulliNB"
predictions = train.predict(model_name, test_data_features)
print sum(predictions)
np.savetxt("data/test_data_" + model_name + "_prediction.csv", predictions,
           delimiter=",")

model_name = "GaussianNB"
predictions = train.predict(model_name, test_data_features)
print sum(predictions)
np.savetxt("data/test_data_" + model_name + "_prediction.csv", predictions,
           delimiter=",")

model_name = "MultinomialNB"
predictions = train.predict(model_name, test_data_features)
print sum(predictions)
np.savetxt("data/test_data_" + model_name + "_prediction.csv", predictions,
           delimiter=",")
