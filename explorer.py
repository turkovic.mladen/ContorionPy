# exploration and description analysis of data
import collections
from datetime import datetime
from scipy import stats
from sklearn import preprocessing
import numpy as np


def top_freq(data_list, num):
    counter = collections.Counter(data_list)

    # print(counter)
    # print(counter.values())
    # print(counter.keys())
    top = counter.most_common(num)

    return top


# Replace low frequency entries with lfq_label
# Frequency determined on first dataset applied also to second
def data_merge_low_freq(data, data2, n_labels, lfq_label):
    # #######

    top_n_data = top_freq(data, n_labels)

    top_names = [x[0] for x in top_n_data]

    # print "Top names list:", top_names
    # print " > count in top_names", sum((x in top_names) for x in data)

    new_data = data
    index = np.where([not (x in top_names) for x in data])
    new_data[index] = lfq_label

    new_data2 = data2
    index2 = np.where([not (x in top_names) for x in data2])
    new_data2[index2] = lfq_label

    return new_data, new_data2

    # ######


def binarise_labels(data):
    lb = preprocessing.LabelBinarizer()
    lb.fit(data)
    # print lb.classes_
    return lb.transform(data)


def describe(data):
    print "Type:", type(data)
    print "Shape:", data.shape
    print "Column names:", data.dtype.names

    print "\n Statistical overview: "
    for arr in data.dtype.names:
        if arr in ['num1', 'num2', 'num3', 'num4', 'num5', 'num6']:
            print arr, stats.describe(data[arr])

    if 'net_revenue' in data.dtype.names:
        print "net_revenue > 0:", sum(data['net_revenue'] > 0)


def to_array(data):
    # convert to array
    return data.view(np.float64).reshape(data.shape)


def transform(train_data, test_data, top_n_labels):
    # ### cat ###

    print "\nBinning cat1..."
    lfq_cat1_data, lfq_cat1_data2 = \
        data_merge_low_freq(train_data['cat1'], test_data['cat1'],
                            top_n_labels, "val_LowFq")

    bin_cat1 = binarise_labels(lfq_cat1_data)
    bin2_cat1 = binarise_labels(lfq_cat1_data2)

    print "\nBinning cat2..."
    lfq_cat2_data, lfq_cat2_data2 = \
        data_merge_low_freq(train_data['cat2'], test_data['cat2'],
                            top_n_labels, "val_LowFq")

    bin_cat2 = binarise_labels(lfq_cat2_data)
    bin2_cat2 = binarise_labels(lfq_cat2_data2)

    print "\nBinning cat3..."
    lfq_cat3_data, lfq_cat3_data2 = \
        data_merge_low_freq(train_data['cat3'], test_data['cat3'],
                            top_n_labels, "val_LowFq")

    bin_cat3 = binarise_labels(lfq_cat3_data)
    bin2_cat3 = binarise_labels(lfq_cat3_data2)

    # ### day_id ###
    # convert into day of the week

    week_day = []
    for x in train_data['day_id']:
        week_day.append(datetime.strptime(x, '%Y%m%d').weekday())

    week_day2 = []
    for x in test_data['day_id']:
        week_day2.append(datetime.strptime(x, '%Y%m%d').weekday())

    # ### make new data ###

    new_train_data = np.stack((
        train_data['net_price'].T, np.asarray(week_day).T,
        train_data['num1'].T, train_data['num2'].T, train_data['num3'].T,
        train_data['num4'].T, train_data['num5'].T, train_data['num6'].T
    ), axis=1)

    new_train_data = np.hstack((new_train_data,
                                bin_cat1, bin_cat2, bin_cat3))

    # new_train_data = np.hstack((new_train_data,
    #                      data['net_revenue'][:, None]))

    # ### New testing data #

    new_test_data = np.stack((
        test_data['net_price'].T, np.asarray(week_day2).T,
        test_data['num1'].T, test_data['num2'].T, test_data['num3'].T,
        test_data['num4'].T, test_data['num5'].T, test_data['num6'].T
    ), axis=1)

    new_test_data = np.hstack((new_test_data, bin2_cat1, bin2_cat2, bin2_cat3))

    # return feature vectors and labels for train data, only features for test data
    return to_array(new_train_data), train_data['net_revenue'], to_array(new_test_data)
