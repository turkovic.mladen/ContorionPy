# load data from file

# load from csv only the first time and create npy
# if npy file exists load that one

import numpy as np
import os.path


def get_file(file_name, force):
    if (not force) and os.path.isfile(file_name + ".npy"):

        print"Loading: ", file_name+".npy"
        data = np.load(file_name + ".npy")

    else:

        print"Loading: ", file_name
        data = np.genfromtxt(file_name,
                             skip_header=1,
                             dtype=[
                                 ('net_price', '<f2'),
                                 ('day_id', '|S8'),
                                 ('num1', '<i4'),
                                 ('num2', '<f8'),
                                 ('num3', '<f8'),
                                 ('num4', '<f8'),
                                 ('num5', '<f8'),
                                 ('num6', '<i4'),
                                 ('cat1', '|S10'),
                                 ('cat2', '|S10'),
                                 ('cat3', '|S10'),
                                 ('net_revenue', '<f8')
                             ],
                             delimiter=",")

        np.save(file_name + ".npy", data)

    return data


def get_predict_file(file_name, force):
    if (not force) and os.path.isfile(file_name + ".npy"):

        print"Loading: ", file_name+".npy"
        data = np.load(file_name + ".npy")

    else:

        print"Loading: ", file_name
        data = np.genfromtxt(file_name,
                             skip_header=1,
                             dtype=[
                                 ('net_price', '<f2'),
                                 ('day_id', '|S8'),
                                 ('num1', '<i4'),
                                 ('num2', '<f8'),
                                 ('num3', '<f8'),
                                 ('num4', '<f8'),
                                 ('num5', '<f8'),
                                 ('num6', '<i4'),
                                 ('cat1', '|S10'),
                                 ('cat2', '|S10'),
                                 ('cat3', '|S10')
                             ],
                             delimiter=",")

        np.save(file_name + ".npy", data)

    return data